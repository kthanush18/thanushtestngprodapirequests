package com.thaunsh.api.test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import io.restassured.RestAssured;
import io.restassured.http.Header;
import io.restassured.http.Headers;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public abstract class ProductAPIRequests {
	private static RequestSpecification myRequest;
	private static Response myResponse;
	private static Header myContentHeader;
	private static Header myAuthHeader;
	private static Headers myMultipleHeaders;
	private static String authToken;
	private static String productID;
	
	protected static void  intiateMyRequest() {
		RestAssured.baseURI = ReadMyProperties.getMyProperty("BASEURL") + ":" + ReadMyProperties.getMyProperty("PORT");
		myRequest = RestAssured.given();
	}
	
	private static Header contentTypeHeader() {
		myContentHeader = new Header("Content-Type", "application/json");
		return myContentHeader;
	}
	
	private static Header authorizationHeader() {
		myAuthHeader = new Header("Authorization", authToken);
		return myAuthHeader;
	}
	
	private static File getFileData(String fileName) {
		File myFile = new File(System.getProperty("user.dir")+"\\ThanushTestData\\"+fileName);
		return myFile;
	}
	
	private static JsonPath loginJSONResponse() {
		myRequest.header(contentTypeHeader());
		myRequest.body(getFileData("LoginTest.txt"));
		myResponse = myRequest.post(ReadMyProperties.getMyProperty("SIGNINAPI"));
		return new JsonPath(myResponse.asString());
	}
	
	protected static String loginAPIRequest() {
		JsonPath loginJsonResponse = loginJSONResponse();
		return loginJsonResponse.getString("status");
	}
	
	private static JsonPath addProductJsonResponse() {
		authToken = loginJSONResponse().getString("data.token");
		List<Header> headers = new ArrayList<Header>();
		headers.add(myContentHeader);
		headers.add(authorizationHeader());
		myMultipleHeaders = new Headers(headers);
		myRequest.headers(myMultipleHeaders);
		myRequest.body(getFileData("AddProductTest.txt"));
		myResponse = myRequest.post(ReadMyProperties.getMyProperty("PRODUCTAPI"));
		return new JsonPath(myResponse.asString());
	}
	
	protected static String addProductAPIRequest() {
		
		JsonPath addProductJsonResponse = addProductJsonResponse();
		productID = addProductJsonResponse.getString("data.id");
		return addProductJsonResponse.getString("status");
	}
	
	private static JsonPath productDetailsJsonResponse() {
		authToken = loginJSONResponse().getString("data.token");
		addProductAPIRequest();
		myRequest.header(myAuthHeader);
		myResponse = myRequest.get(ReadMyProperties.getMyProperty("PRODUCTAPI")+"/"+productID);
		return new JsonPath(myResponse.asString());
	}
	
	protected static String getProductName() {
		return productDetailsJsonResponse().getString("prod_name");
	}
	protected static String getProductDesc() {
		return productDetailsJsonResponse().getString("prod_desc");
	}
	protected static String getProductPrice() {
		return productDetailsJsonResponse().getString("prod_price");
	}
	
	protected static String updateProductDetails() {
		authToken = loginJSONResponse().getString("data.token");
		addProductAPIRequest();
		myRequest.headers(myMultipleHeaders);
		myRequest.body(getFileData("UpdateProductTest.txt"));
		myResponse = myRequest.put(ReadMyProperties.getMyProperty("PRODUCTAPI")+"/"+productID);
		JsonPath updateProductJsonResponse = new JsonPath(myResponse.asString());
		return updateProductJsonResponse.getString("status");
	}
	
	protected static String deleteProductDetails() {
		authToken = loginJSONResponse().getString("data.token");
		addProductAPIRequest();
		myRequest.header(myAuthHeader);
		myResponse = myRequest.delete(ReadMyProperties.getMyProperty("PRODUCTAPI")+"/"+productID);
		JsonPath deleteProductJsonResponse = new JsonPath(myResponse.asString());
		return deleteProductJsonResponse.getString("status");
	}
}
