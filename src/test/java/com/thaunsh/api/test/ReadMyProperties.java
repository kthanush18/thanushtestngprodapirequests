package com.thaunsh.api.test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class ReadMyProperties {

	private static Properties myProperty;
	private static FileReader myFileReader;

	public static String getMyProperty(String propertyName) {

		myProperty = new Properties();
		File propertyFile = new File(System.getProperty("user.dir") + "\\config\\myConfig.properties");
		try {
			myFileReader = new FileReader(propertyFile);
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}
		try {
			myProperty.load(myFileReader);
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		return myProperty.getProperty(propertyName);
	}

}
