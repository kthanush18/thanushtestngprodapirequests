package com.prodAPI.testNG;

import static org.testng.Assert.assertEquals;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.thaunsh.api.test.ProductAPIRequests;

public class ProdAPItests extends ProductAPIRequests {
	
	@BeforeTest
	public void AddBaseURLToRequest() {
		intiateMyRequest();
	}

	@Test (testName = "loginAPITest", description = "Request for login into products page")
	public void LoginAPIRequest() {
		assertEquals(loginAPIRequest(), "success");
	}
	
	@Test (testName = "addProductAPITest", description = "Add a product to the products table")
	public void AddProductAPIRequest() {
		assertEquals(addProductAPIRequest(), "success");
	}
	
	@Test (testName = "ProductDetailsTest", description = "Get productName, productDesc and Price")
	public void GetProductDetails() {
		assertEquals(getProductName(), "thanush_test");
		assertEquals(getProductDesc(), "testing_post_request");
		assertEquals(getProductPrice(), "499.00");
	}
	
	@Test (testName =  "UpdateProductAPITest", description = "update a product in the products table")
	public void UpdateProductAPIRequest() {
		assertEquals(updateProductDetails(), "success");
	}
	
	@Test (testName =  "DeleteProductAPITest", description = "deletes a product in the products table")
	public void DeleteProductAPIRequest() {
		assertEquals(deleteProductDetails(), "success");
	}
	
	@AfterTest
	public void messageAfterAllTests() {
		System.out.println("Products API tests are completed!!!");
	}
}
